<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */

namespace Znrl\LotteryNumbersBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ZnrlLotteryNumbersBundle extends Bundle
{
}
