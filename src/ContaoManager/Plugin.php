<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */

namespace Znrl\LotteryNumbersBundle\ContaoManager;

use Znrl\LotteryNumbersBundle\ZnrlLotteryNumbersBundle;
use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ZnrlLotteryNumbersBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
