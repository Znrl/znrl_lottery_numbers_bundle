<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */


/**
 * BACK END MODULES
 */

$GLOBALS['BE_MOD']['content']['znrl_lottery_numbers'] = array(
    'tables'     => array('tl_znrl_lottery_numbers')
);

/**
 * FRONT END MODULES
 */
 
$GLOBALS['FE_MOD']['content']['znrl_lottery_numbers'] = 'Znrl\LotteryNumbersBundle\ModuleZnrlLotteryNumbers';

/**
 * MODEL MAPPINGS
 */

$GLOBALS['TL_MODELS']['tl_znrl_lottery_numbers'] = 'Znrl\LotteryNumbersBundle\LotteryNumbersModel';
