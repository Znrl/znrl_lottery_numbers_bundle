<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */


/**
 * Add palettes to tl_module
 */
 
$GLOBALS['TL_DCA']['tl_module']['palettes']['znrl_lottery_numbers'] = '{title_legend},name,headline,type;{znrl_lottery_numbers_config_legend},znrl_lottery_numbers;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
 
/**
 * Add field to tl_module
 */
 
 $GLOBALS['TL_DCA']['tl_module']['fields']['znrl_lottery_numbers'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['znrl_lottery_numbers'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'foreignKey'              => 'tl_znrl_lottery_numbers.title',
    'sql'                     => "varchar(255) NOT NULL default ''",
    'eval'                    => array('multiple'=>false, 'mandatory'=>true, 'tl_class'=>'w50')
);