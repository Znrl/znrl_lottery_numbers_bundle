<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */


/**
 * Table tl_znrl_lottery_numbers
 */
$GLOBALS['TL_DCA']['tl_znrl_lottery_numbers'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('title'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif'
            )
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('type','use_bonus_numbers'),
        'default'                     => '{title_legend},title,type;{options_legend},picks_max,numbers_sort,numbers_allow_same,show_odds;{numbers_picker_legend},numbers_min,numbers_max,numbers_quantity;{bonus_numbers_picker_legend},use_bonus_numbers',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        'use_bonus_numbers'      => 'bonus_numbers_min,bonus_numbers_max,bonus_numbers_quantity,bonus_numbers_same_for_all'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['title'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'flag'                    => 1,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'maxlength' => 255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'type' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['type'],
            'exclude'                 => true,
            'sorting'                 => true,
            'filter'                  => true,
            'inputType'               => 'select',
            'options'                 => array('customizable','predefined'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['type'],
            'eval'                    => array('mandatory' => true, 'includeBlankOption' => true, 'submitOnChange' => true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'picks_max' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['picks_max'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'minval' => '1', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'numbers_sort' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['numbers_sort'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class' => 'w50 m12 cbx', 'chosen' => true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'numbers_allow_same' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['numbers_allow_same'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class' => 'w50 m12 cbx'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'show_odds' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['show_odds'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class' => 'w50 m12 cbx'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'numbers_quantity' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['numbers_quantity'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'minval' => '1', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'numbers_min' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['numbers_min'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'numbers_max' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['numbers_max'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'minval' => '1', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'use_bonus_numbers' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['use_bonus_numbers'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true, 'tl_class' => 'w50 m12 cbx'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'bonus_numbers_quantity' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['bonus_numbers_quantity'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'minval' => '1', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'bonus_numbers_min' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['bonus_numbers_min'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'bonus_numbers_max' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['bonus_numbers_max'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'rgxp' => 'natural', 'minval' => '1', 'nospace'=> true),
            'sql'                     => "INT unsigned NULL"
        ),
        'bonus_numbers_same_for_all' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_lottery_numbers']['bonus_numbers_same_for_all'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class' => 'w50 m12 cbx'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
    )
);
