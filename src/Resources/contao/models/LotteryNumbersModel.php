<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */


/**
 * Namespace
 */

namespace Znrl\LotteryNumbersBundle;


/**
 * Model LotteryNumbers
 */


class LotteryNumbersModel extends \Model
{
    protected static $strTable = 'tl_znrl_lottery_numbers';
}