<?php

/*
 * This file is part of ZnrlLotteryNumbersBundle.
 *
 * (c) Lorenz Ketterer
 *
 * @license LGPL-3.0-or-later
 */


/**
 * Namespace
 */
namespace Znrl\LotteryNumbersBundle;

use Contao;

/**
 * Class ModuleLotteryNumbers
 *
 */

class ModuleZnrlLotteryNumbers extends \Module
{
    
    /**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'fe_znrl_lottery_numbers';
    
    /**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate() {
	    if (TL_MODE == 'BE') {
	        $objTemplate = new \BackendTemplate('be_wildcard');
	        $objTemplate->wildcard = '### LOTTERY NUMBERS MODUL ###';
	        $objTemplate->title = $this->headline;
	        $objTemplate->id = $this->id;
	        $objTemplate->link = $this->name;
	        $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
	        
	        return $objTemplate->parse();
	    }
        
	    return parent::generate();
    }
    
    protected function compile() {
        
        $picks = array();
        
        $this->compileLotteryDetails();
        
        if (!is_null($this->picks_quantity) && $this->picks_quantity > 0) {
            
            for ($i = 0; $i < $this->picks_quantity; $i++){
            
                $picks[$i]['numbers'] = $this->pickNumbers($this->numbers_min, $this->numbers_max, $this->numbers_quantity);
            }
            if ($this->bonus_numbers_quantity >= 1) {
                
                if ($this->bonus_numbers_same_for_all == 1) {
                    
                    $picks['bonus_numbers'] = $this->pickNumbers($this->bonus_numbers_min, $this->bonus_numbers_max, $this->bonus_numbers_quantity);
                }
                else {
                    for ($i = 0; $i < $this->picks_quantity; $i++){

                        $picks[$i]['bonus_numbers'] = $this->pickNumbers($this->bonus_numbers_min, $this->bonus_numbers_max, $this->bonus_numbers_quantity);
                    }
                }
            }
        }
        
        $this->Template->type = $this->type;
        $this->Template->picks_max = $this->picks_max;
        $this->Template->picks_quantity = $this->picks_quantity;
        $this->Template->numbers_sort = $this->numbers_sort;
        $this->Template->numbers_allow_same = $this->numbers_allow_same;
        $this->Template->numbers_quantity_form_set = $this->numbers_quantity_form_set;
        $this->Template->numbers_quantity = $this->numbers_quantity;
        $this->Template->numbers_min_form_set = $this->numbers_min_form_set;
        $this->Template->numbers_min = $this->numbers_min;
        $this->Template->numbers_max_form_set = $this->numbers_max_form_set;
        $this->Template->numbers_max = $this->numbers_max;
        
        if ($this->use_bonus_numbers === 1) {
            
            $this->Template->use_bonus_numbers = $this->use_bonus_numbers;
            $this->Template->bonus_numbers_quantity_form_set = $this->bonus_numbers_quantity_form_set;
            $this->Template->bonus_numbers_quantity = $this->bonus_numbers_quantity;
            $this->Template->bonus_numbers_min_form_set = $this->bonus_numbers_min_form_set;
            $this->Template->bonus_numbers_min = $this->bonus_numbers_min;
            $this->Template->bonus_numbers_max_form_set = $this->bonus_numbers_max_form_set;
            $this->Template->bonus_numbers_max = $this->bonus_numbers_max;
            $this->Template->bonus_numbers_same_for_all = $this->bonus_numbers_same_for_all;
        }
        
        $this->Template->picks = $picks;
        $this->Template->action = \Environment::get('indexFreeRequest');
	}
    
    protected function compileLotteryDetails() {
        
        $objLotteryNumbers = LotteryNumbersModel::findByPk($this->znrl_lottery_numbers);
        
        $this->type = $objLotteryNumbers->type;
        $this->use_bonus_numbers = $objLotteryNumbers->use_bonus_numbers;
        $this->picks_max = $objLotteryNumbers->picks_max;
        
        $picks_quantity = intval(\Input::get('picks_quantity'));

        if ($picks_quantity != 0 && $picks_quantity < 1){
        
            $this->picks_quantity = 1;
        }
        elseif ($picks_quantity > $objLotteryNumbers->picks_max) {
    
            $this->picks_quantity = $objLotteryNumbers->picks_max;
        }
        else {
        
            $this->picks_quantity = $picks_quantity;
        }
        
        $numbers_sort = intval(\Input::get('numbers_sort'));
        
        if ($numbers_sort === 1 || $numbers_sort === 0) {
            
            $this->numbers_sort = $numbers_sort;
        }
        else {
        
            $this->numbers_sort = $objLotteryNumbers->numbers_sort;
        }
        
        $this->numbers_show_odds = $objLotteryNumbers->numbers_show_odds;
        
        if ($this->type == 'customizable') {
            
            $numbers_allow_same = intval(\Input::get('numbers_allow_same'));
        
            if ($numbers_allow_same === 1 || $numbers_allow_same === 0) {
                
                $this->numbers_allow_same = $numbers_allow_same;
            }
            else {
            
                $this->numbers_allow_same = $objLotteryNumbers->numbers_allow_same;
            }

            $this->numbers_quantity_form_set = $objLotteryNumbers->numbers_quantity;
            $numbers_quantity = intval(\Input::get('numbers_quantity'));
            
            if ($numbers_quantity > $objLotteryNumbers->numbers_quantity) {
        
                $this->numbers_quantity = $objLotteryNumbers->numbers_quantity;
            }
            elseif ($numbers_quantity < 1){
            
                $this->numbers_quantity = 1;
            }
            else {
            
                $this->numbers_quantity = $numbers_quantity;
            }

            $this->numbers_min_form_set = $objLotteryNumbers->numbers_min;
            $numbers_min = intval(\Input::get('numbers_min'));
            
            if (is_null($numbers_min) || $numbers_min < 0){
            
                $this->numbers_min = 0;
            }
            if ($numbers_min < $objLotteryNumbers->numbers_min) {
        
                $this->numbers_min = $objLotteryNumbers->numbers_min;
            }
            else {
            
                $this->numbers_min = $numbers_min;
            }

            $this->numbers_max_form_set = $objLotteryNumbers->numbers_max;
            $numbers_max = intval(\Input::get('numbers_max'));
            
            if (is_null($numbers_max) || $numbers_max < 1){
            
                $this->numbers_max = 1;
            }
            if ($numbers_max > $objLotteryNumbers->numbers_max) {
        
                $this->numbers_max = $objLotteryNumbers->numbers_max;
            }
            else {
            
                $this->numbers_max = $numbers_max;
            }
            
            if ($this->numbers_min > $this->numbers_max) {
                
                $this->numbers_min = $this->numbers_max - 1;
            }
            
            if ($this->use_bonus_numbers) {
                
                $this->bonus_numbers_quantity_form_set = $objLotteryNumbers->bonus_numbers_quantity;
                $bonus_numbers_quantity = intval(\Input::get('bonus_numbers_quantity'));
                
                if ($bonus_numbers_quantity < 1){
                
                    $this->bonus_numbers_quantity = 0;
                    
                    return;
                }
                elseif ($bonus_numbers_quantity > $objLotteryNumbers->bonus_numbers_quantity) {
            
                    $this->bonus_numbers_quantity = $objLotteryNumbers->bonus_numbers_quantity;
                }
                else {
                
                    $this->bonus_numbers_quantity = $bonus_numbers_quantity;
                }
                
                $this->bonus_numbers_min_form_set = $objLotteryNumbers->bonus_numbers_min;
                $bonus_numbers_min = intval(\Input::get('bonus_numbers_min'));
                
                if (is_null($bonus_numbers_min) || $bonus_numbers_min < 0){
                
                    $this->bonus_numbers_min = 0;
                }
                elseif ($bonus_numbers_min < $objLotteryNumbers->bonus_numbers_min) {
            
                    $this->bonus_numbers_min = $objLotteryNumbers->bonus_numbers_min;
                }
                else {
                
                    $this->bonus_numbers_min = $bonus_numbers_min;
                }
                
                $this->bonus_numbers_max_form_set = $objLotteryNumbers->bonus_numbers_max;
                $bonus_numbers_max = intval(\Input::get('bonus_numbers_max'));
                
                if (is_null($bonus_numbers_max) || $bonus_numbers_max < 1){
                
                    $this->bonus_numbers_max = 1;
                }
                if ($bonus_numbers_max > $objLotteryNumbers->bonus_numbers_max) {
            
                    $this->bonus_numbers_max = $objLotteryNumbers->bonus_numbers_max;
                }
                else {
                
                    $this->bonus_numbers_max = $bonus_numbers_max;
                }
                
                if ($this->bonus_numbers_min > $this->bonus_numbers_max) {
                
                    $this->bonus_numbers_min = $this->bonus_numbers_max - 1;
                }
                
                $bonus_numbers_same_for_all = intval(\Input::get('bonus_numbers_same_for_all'));
        
                if ($bonus_numbers_same_for_all === 1 || $bonus_numbers_same_for_all === 0) {
                    
                    $this->bonus_numbers_same_for_all = $bonus_numbers_same_for_all;
                }
                else {
                
                    $this->bonus_numbers_same_for_all = $objLotteryNumbers->bonus_numbers_same_for_all;
                }
            }
        }
        else {
            
            $this->numbers_allow_same = $objLotteryNumbers->numbers_allow_same;
            $this->numbers_quantity = $objLotteryNumbers->numbers_quantity;
            $this->numbers_min = $objLotteryNumbers->numbers_min;
            $this->numbers_max = $objLotteryNumbers->numbers_max;
            
            if ($this->use_bonus_numbers) {
                
                $this->bonus_numbers_quantity = $objLotteryNumbers->bonus_numbers_quantity;
                $this->bonus_numbers_min = $objLotteryNumbers->bonus_numbers_min;
                $this->bonus_numbers_max = $objLotteryNumbers->bonus_numbers_max;
                $this->bonus_numbers_same_for_all = $objLotteryNumbers->bonus_numbers_same_for_all;
            }
        }
    }
    
    protected function pickNumbers(int $min, int $max, int $quantity) {

        $numbers = array();
    
        for ($i = 0; $i < $quantity; ) {
        
            $number = random_int($min, $max);
            
            if (!in_array($number, $numbers) || $this->numbers_allow_same === 1) {
                
                $numbers[$i] = $number;
                $i++;
            }
        }
            
        if ($this->numbers_sort === 1) {
            
            asort($numbers);
        }
        
        return $numbers;
    }
}
